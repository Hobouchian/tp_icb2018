# -*- coding: utf-8 -*-

class Laberinto(object):
    def __init__(self, parent=None):
        self.parent=parent
        self.rata=(0,0)
        self.queso=(5,5)
        self.filas=0
        self.columnas=0
        self.matriz=None
        self.dimensiones=None
        self.caminoActual=[self.rata]
        self.visitadas=[self.rata]
        
    def cargar(self,fn):
        lb=open(fn)
        dimensiones=[]
        lab=[]
        for linea in lb:
            lab.append(linea)
        dimensiones = lab[0].lstrip("Dim(").lstrip("[").rstrip(")\n").rstrip("]\n").split("][")
        dimensiones=dimensiones[0].split(",")
        dimensiones = (int(dimensiones[0]),int(dimensiones[1]))
        self.dimensiones=dimensiones
        laberinto = []
        for i in range(1,len(lab)):
            renglon = lab[i][1:len(lab[i])-2].split("][") 
            fila = []
            for c in renglon:
                elems = c.split(",")
                celda = []
                for e in elems:
                    celda.append(int(e))
                fila.append(celda)
            laberinto.append(fila)
        self.matriz=laberinto
        self.filas=dimensiones[0]
        self.columnas=dimensiones[1]
                 
    def getPosicionRata(self):#Recorre todas las celdas hasta encontrar la rata          
        for i in range(self.tamano()[0]):
            for j in range(self.tamano()[1]):
                if self.esPosicionRata(i, j) is True:
                    return (i, j)
    
    def tamano(self):
        return (self.filas, self.columnas)
        
    def getPosicionQueso(self):
        for i in range(self.tamano()[0]):
            for j in range(self.tamano()[1]):
                if self.esPosicionQueso(i, j) is True:
                    return (i, j)
    
    def get(self,i,j):
        izq=self.matriz[i][j][0]==1
        arriba=self.matriz[i][j][1]==1  
        der=self.matriz[i][j][2]==1
        abajo=self.matriz[i][j][3]==1
        return [izq,arriba,der,abajo]
    
    def setPosicionRata(self,i,j): #Devuelve True si el movimiento es posible segun el (i,j) exista dentro de la matriz, y si la celda destino no ha sido visitada 
        if i in range(self.filas) and j in range(self.columnas): 
            if (i,j) not in self.visitadas: # si la celda destino no fue visitada
                self.rata=(i,j) #mueve la rata
                self.visitadas.append((i,j)) #agrega la celda a la lista de celdas visitadas
                self.caminoActual.append((i,j)) # si la celda no fue visitada es a priori caminoActual
                return True 
            else: # esto sucede solo cuando en la funcion resolver no se puede mover en ninguna direccion ya que todas las celdas adyacentes fueron visitadas
                self.rata=(i,j) # por como esta planteada resolver(), i,j son la celda anterior que visito
                self.caminoActual.pop() #sacamos de caminoActual el ultimo elemento ya que lo visito y retrocedio
                return True 
        else:
            return False
        
    def setPosicionQueso(self,i,j):
        if i in range(self.filas) and j in range(self.columnas): 
            self.queso=(i,j)
            return True
        else:
            return False

    def esPosicionRata(self, i, j): # devuelve True si el par ordenado determinado por (i; j) corresponde a la posicion de la rata, False en caso contrario.
        return self.rata==(i,j) 
    
    def esPosicionQueso(self, i, j): # devuelve True si el par ordenado determinadopor (i; j) corresponde a la posicion del queso, False en caso contrario.
        return self.queso==(i,j) 
    
    def getInfoCelda(self,i,j):
        if (i,j) in self.visitadas:
            if (i,j) in self.caminoActual:
                return {'visitada': True, 'caminoActual':True }
            else:
                return {'visitada': True, 'caminoActual':False }
        else:
            return {'visitada': False, 'caminoActual':False }
            
    def resetear(self):
        self.caminoActual=[self.rata]
        self.visitadas=[self.rata]
    
    def resuelto(self):  # devuelve True si el laberinto esta resuelto, es decir, si la posicion de la rata es la misma que la del queso, y False en otro caso.
        return self.rata==self.queso

    def __redibujar(self):
	    if self.parent is not None:
	        self.parent.update()
  
    def __esMovimientoPosible(self,i,j): #Devuelve True si el movimiento es posible porque no hay pared en esa direccion
        if not (i in range(self.filas) and j in range(self.columnas)):
            return False
        paredesCelda=self.get(self.rata[0],self.rata[1])
        if self.rata[0]!=i:
            if i<self.rata[0]: #se quiso mover hacia arriba
                return paredesCelda[1]==False # Es true si no hay pared y false si la hay
            if i>self.rata[0]:#se quiso mover hacia abajo
                return paredesCelda[3]==False
        elif self.rata[1]!=j: 
            if j<self.rata[1]: #se quiso mover hacia la izq
                return paredesCelda[0]==False # Es true si no hay pared y false si la hay
            if j>self.rata[1]:#se quiso mover hacia la der
                return paredesCelda[2]==False
        else:
            return 'La rata no se puede mover a su misma celda'
                
    def resolver(self): #resuelve el laberinto utilizando backtracking, Devuelve True si llega a la posicion del queso, False si esto no es posible (si no existe un camino).
        posRata = self.rata
        print(posRata)
        if self.resuelto():
            return True 
        if self.__esMovimientoPosible(self.rata[0], (self.rata[1]+1)) and self.getInfoCelda(self.rata[0],(self.rata[1]+1))['visitada']==False: # si se puede mover hacia la derecha porque no hay pared ni fue visitada
            self.setPosicionRata(self.rata[0],(self.rata[1]+1)) #cambia posicion rata y listas celdas visitadas y camino actual
            self.__redibujar()
            if self.resolver(): # se va a seguir moviendo hacia la derecha siempre que no haya pared, ni sea una celda ya visitada, ni se resuelva. Si resuelve da True, si choca pared  o celda visitada continua hacia arriba
                return True
            else:
                self.setPosicionRata(posRata[0],posRata[1])
                
        if self.__esMovimientoPosible((self.rata[0]-1), self.rata[1]) and self.getInfoCelda((self.rata[0]-1), self.rata[1])['visitada']==False:# si se puede mover para arriba
            self.setPosicionRata((self.rata[0]-1),self.rata[1]) #cambia posicion rata y listas celdas visitadas y camino actual
            self.__redibujar()
            if self.resolver(): 
                return True
            else:
                self.setPosicionRata(posRata[0],posRata[1])

        if self.__esMovimientoPosible(self.rata[0], (self.rata[1]-1)) and self.getInfoCelda(self.rata[0], (self.rata[1]-1))['visitada']==False:# si se puede mover para la izquierda
             self.setPosicionRata(self.rata[0],(self.rata[1]-1)) #cambia posicion rata y listas celdas visitadas y camino actual
             self.__redibujar()
             if self.resolver(): 
                return True
             else:
                self.setPosicionRata(posRata[0],posRata[1])
                
        if self.__esMovimientoPosible((self.rata[0]+1), self.rata[1]) and self.getInfoCelda((self.rata[0]+1), self.rata[1])['visitada']==False:# si se puede mover para abajo
             self.setPosicionRata((self.rata[0]+1),self.rata[1]) #cambia posicion rata y listas celdas visitadas y camino actual
             self.__redibujar()
             if self.resolver(): 
                return True
             else:
                self.setPosicionRata(posRata[0],posRata[1])
        
        return False


         
'''
#Pruebas:    
lab=Laberinto()
lab.cargar('laberinto15x30.lab')
lab.matriz
lab.getPosicionRata()
lab.tamano()
lab.getPosicionQueso()
lab.get(lab.rata[0],lab.rata[1])
lab.setPosicionRata(0,1)
lab.caminoActual
lab.visitadas
lab.setPosicionRata(0,0)
lab.caminoActual
lab.visitadas
lab.setPosicionQueso(9,7)
lab.getInfoCelda(0,1)
lab.esMovimientoPosible(lab.rata[0],(lab.rata[1]+1))
lab.resetear()
lab.rata
lab.queso
lab.resolver()
lab.resuelto()
'''
