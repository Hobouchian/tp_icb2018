#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Gomez, Gimena y Hobouchian, Maria Paula
"""
#Ejercicio 3 del TP icb 2018
import sys
import imfilters
from scipy import misc
import numpy as np
from time import time

def gray_filter(a):
    #Filtro de escala de grises sobre img a color (se usa empty porque posteriormente 
    #se llenan todos los elementos de la imagen)
    res=np.empty((a.shape[0],a.shape[1]))
    for i in range(res.shape[0]):
        for j in range(res.shape[1]):
            res[i,j]=0.3*a[i,j,0]+0.6*a[i,j,1]+0.11*a[i,j,2]
    return res

def blur_filter(a):
    #Filtro de difuminado sobre img en escala de grises (en este caso se usa ceros
    #para valores no alcanzados por la condicion que deben quedar en negro)
    res=np.zeros((a.shape[0],a.shape[1]))
    for i in range(1,res.shape[0]-1):
        for j in range(1,res.shape[1]-1):
            res[i,j]=(a[i-1,j]+a[i+1,j]+a[i,j-1]+a[i,j+1])/4
    return res

if __name__ == '__main__':
    #Se cargan distintas imagenes para la comparacion del experimiento 
    #En terminal: python3 ej3.py /home/mpaula/tp_icb2018/Ej3 8 cultura ciencia derechos arbol diversidad satelite cerro laguna
    n=int(sys.argv[2])+3
    for i in range(3,n):
        entrada=sys.argv[1]+'/imagenes/'+sys.argv[i]+'.jpg'
        a=misc.imread(entrada)
        #Tiempo que demora la implementacion usando python
        ti=time()
        respy=blur_filter(gray_filter(a))
        tf=time()
        tpy=float("{:.4f}".format(tf-ti))
        #Tiempo que demora la implementacion usando C++
        ti=time()
        rescc=imfilters.blur_filter(imfilters.gray_filter(a))
        tf=time()
        tcc=float("{:.4f}".format(tf-ti))
        print("Demora con Python (s): ",tpy," y con C++: ",tcc," imagen (pixeles):",sys.argv[i], " alto: ",a.shape[0]," y ancho: ",a.shape[1])
        #Almacenar ambas imagenes difuminadas
        salida=sys.argv[1]+'/filtros/'+sys.argv[i]
        misc.imsave(salida+'_py.jpg',respy)
        misc.imsave(salida+'_cc.jpg',rescc)
        #Verificacion de ambas implementaciones
        if (abs(respy-rescc)<0.0001).all():
            print("El resultado de ambas implementaciones coincide")
        else:
            print("Revisar las implementaciones ya que no coinciden")