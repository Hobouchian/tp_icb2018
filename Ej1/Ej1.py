#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 10:17:41 2018

@author: icb05
"""
# TP grupal, ej 1 
#Se ejecuta desde la terminal como python Ej1.py (o python3 Ej1.py)
# El numero de agumentos debe ser menor o igual a 3, siendo:
# argv[1] una entrada de puntos (formato csv) o un numero entero mayor o igual a 2 (minima longitud de lista random de puntos); 
# y siendo argv[2] un numero entero mayor a 2 (maxima longitud de la lista random de puntos)'

import sys
import math 
import time
import random
import numpy as np
import matplotlib.pyplot as plt

#1 
#funcion que crea una lista de tuplas/puntos (x,y) a partir del archivo de entrada

def listaDePuntos(fn):
    entrada=open(fn)
    lista=[]
    for linea in entrada:
        punto=linea.split(',')
        lista.append((float(punto[0]), float(punto[1])))
    return lista

#lista_puntos=listaDePuntos("lista_puntos.csv")


#2 Encontrar los dos puntos cuya distancia sea mínima por FUERZA BRUTA

def distanciaEntrePuntos(p1,p2): # siendo p1 y p2 tuplas (x,y) calcula la distancia euclidea
    return math.sqrt(((p1[0]-p2[0])**2)+((p1[1]-p2[1])**2))

def distanciaMinima(a):
    # devuelve los puntos y la distancia, entre los puntos con menor distancia de la lista por FUERZA BRUTA
    if len(a)<2:
        return 'La lista tiene menos de dos elementos'
    elif len(a)<3 and len(a)>=2:
        return (a[0],a[1]), distanciaEntrePuntos(a[0],a[1])       
    else:
        j=0
        dMinima=float('inf')
        while j <len(a)-1:
            p1=a[j]
            i=j+1
            while i<len(a):
                p2=a[i]
                dEntrePuntos=distanciaEntrePuntos(p1,p2)
                if dEntrePuntos<dMinima:
                    dMinima=dEntrePuntos
                    punto1=p1
                    punto2=p2
                i+=1
            j+=1
        return (punto1,punto2),dMinima

#Prueba
#puntos=distanciaMinima(lista_puntos)[0] #ok
#MinDist=distanciaMinima(lista_puntos)[1] #ok

#3a
# MERGE: La funcion toma dos listas ordenadas de manera creciente, y crea una lista output con los elementos
# de ambas listas ordenados de manera creciente segun el valor de x.

def merge(a,b):# requiere que a y b sean listas de tuplas (x,y) ordenadas crecientes
    res=[]
    i=0
    j=0
    while i<len(a) and j<len(b): #puede que una de las listas se termine de adicionar a res y quede la otra, sale del bucle
        if a[i][0]<b[j][0]:
            res.append(a[i])
            i+=1
        else:
            res.append(b[j])
            j+=1
    while i<len(a): #si todavia quedan elementos de a sin adicionar pero no de b
        res.append(a[i])
        i+=1
    while j<len(b):#si todavia quedan elementos de b sin adicionar pero no de a
        res.append(b[j])
        j+=1
    return res #ok

def mergeSort(ls): 
    # toma una lista, la divide en dos, ordena cada mitad y junta ambas mitades utilizando la funcion merge. No modifica a
    if len(ls)<2:
        return ls
    else:
        iMedio=len(ls)//2 #iMedio es la posicion en la mitad de la lista
        priMitadLs=ls[:iMedio]
        segMitadLs=ls[iMedio:]
        a=mergeSort(priMitadLs) #ordeno cada mitad
        b=mergeSort(segMitadLs)
        lsCompleta=merge(a,b) #junto ambas mitades
        return lsCompleta

# UPSORT: Ordena una lista a de tuplas (x,y) segun x creciente intercambiando el mayor numero del segmento 
#de lista no ordenado por el elemento que se encuentra al final de la lista no ordenada 
        
def upSort(a):# requiere que a sea una lista de tuplas (x,y), modifica a
    actual=len(a)-1
    m = 0
    while (actual>0):
        m = posMax(a,actual)
        if a[m][0]>a[actual][0]:
            a[m],a[actual]=a[actual],a[m]
        actual-=1
    return a

def posMax(a,posicionFinal):# requiere que a sea una lista de tuplas (x,y)
    # posMax dice en que posicion esta el numero mayor o maximo
    max=a[0][0]
    posMax=0
    i=0
    while i< len(range(posicionFinal)):
        if a[i][0]>max:
            max=a[i][0]
            posMax=i
        i+=1
    return posMax

#Pruebas        
#ListaOrdenada=mergeSort(lista_puntos) # ok, no modifica lista_puntos
#ListaOrdenada2=upSort(lista_puntos) #ok, modifica lista_puntos            


#3.bcd
    
def distanciaMinimaRec(a):# a debe estar ordenada en x
# calcula la min distancia recursiva en cada mitad de lista y devuelve la menor de las dos en cada division de lista
   if len(a)<=3:#cuando la lista tiene 3 o menos elementos compara por fuerza bruta es decir distanciaMinima()
       return distanciaMinima(a)[0],distanciaMinima(a)[1]
   else:    
       iMedio=len(a)//2
       priMitad=distanciaMinimaRec(a[:iMedio])
       segMitad=distanciaMinimaRec(a[iMedio:])
       if priMitad[1]<segMitad[1]: #compara los valores de distancia
           return priMitad # devuelve los puntos y la distancia
       else:
           return segMitad

#Prueba
#dMinMitades=distanciaMinimaRec(lista_puntos)#ok, requiere lista ordenada

def menorDist(a): # calcula la menor distancia de la lista completa sin evaluar por fuerza bruta toda la lista sino una parte
    # utiliza la minima distancia obtenida por recursion para setear un intervalo
    # definido como los puntos mas lejos en x del punto medio para los cuales 
    #la distancia en x no supere la distancia obtenida por recursion
    #devuelve los puntos,  no la distancia
    if len(a)<=3:#cuando la lista tiene 3 o menos elementos compara por fuerza bruta es decir distanciaMinima()
       return distanciaMinima(a)[0],distanciaMinima(a)[1]
    else:
        iMedio=len(a)//2
        minMitades=distanciaMinimaRec(a) #no compara elementos entre si que esten a mas de tres lugares segun su orden de x
        iMin=iMedio-1
        while iMin>0:
            dist=a[iMedio][0]-a[iMin][0]
            if dist<minMitades[1]:#siempre que la distancia en x entre los dos puntos sea menor a la dist minima encontrada en las mitades
                limInf=iMin
                iMin-=1 #sigo ampliando el intervalo hacia abajo
            else:
                limInf=iMin+1 # si la dist en x supero la min dist en las mitades, no seguir ampliando el intervalo, ese valor es el limite inferior
                iMin=-1 #rompo el bucle
        iMax=iMedio+1
        while iMax<len(a): #idem pero para arriba
            dist=a[iMax][0]-a[iMedio][0]
            if dist<minMitades[1]:
                limSup=iMax
                iMax+=1
            else:
                limSup=iMax-1
                iMax=len(a) 
        if (iMedio-limInf>0) or (limSup-iMedio>0): #si las dist en x entre los numeros en iMedio y iMedio-1 y entre iMedio y iMedio+1 ya superan la minima obtenida en alguna mitad
            intervalo=a[limInf:limSup+1] #dentro de ese intervalo utilizar fuerza bruta
            minIntervalo=distanciaMinima(intervalo)#esto es F bruta
            if minIntervalo[1]<minMitades[1]:
                return minIntervalo[0]
            else:
                return minMitades[0]
        else:
            return minMitades[0]
    
#Prueba
#lista=mergeSort(lista_puntos)
#minD=menorDist(lista)

def distMinDyC(a, algoritmo):
    # esta funcion es la que llama a todas las otras, 
    #es decir recibe una lista a y un algoritmo de ordenamiento y devuelve
    #los puntos que estan a menor distancia
    if algoritmo == 'fBruta':
        return distanciaMinima(a)
    elif algoritmo == mergeSort:
        mergeSort(a)
        return menorDist(a)
    elif algoritmo == upSort:
        upSort(a)
        return menorDist(a)
    else:
        return 'El algoritmo no es correcto'
   
    
        
#Prueba     
#print(distMinDyC(lista_puntos,mergeSort)) #ok

#4

def generarPuntosEntrada(x): # x es la longitud de mi lista de tuplas
        lista=[]
        for i in range(x):
            y=round(random.random()*random.randint(-10,10),2)
            z=round(random.random()*random.randint(-10,10),2)
            lista.append((y,z))
        return lista

#Prueba
#res=generarPuntosEntrada(8) #ok

def medTiempos(a,algoritmo):
    t0=time.time()
    distMinDyC(a,algoritmo)
    t1=time.time()
    return t1-t0

def datosYGrafico(x=2,y=500):# x es la menor len de la lista, debe ser al menos 2; y es el max len de la lista
    #2 y 500 son default arguments, son sustituidos si otros valores para estos argumentos son especificados
    if x<2 or x>y:
        return 'x debe ser mayor o igual a 2; y debe ser mayor que x'
    else:
        longLista=x
        data=[]
        while longLista<=y:
            fila=[longLista]
            lista=generarPuntosEntrada(longLista)
            fila.append(medTiempos(lista,'fBruta'))
            fila.append(medTiempos(lista,mergeSort))
            fila.append(medTiempos(lista,upSort))
            data.append(fila)
            if longLista<50:
                longLista+=1
            elif longLista<500:
                longLista+=10
            else:
                longLista+=100
        data=np.matrix(data)
        plt.xlabel('Tamaño del conjunto de puntos') #grafico tpo vs cant de puntos para los tres opciones 
        plt.ylabel('Tiempo de ejecución (s)')
        plt.plot(data[:,0], data[:,1], label='Fuerza bruta', linewidth=3)
        plt.plot(data[:,0], data[:,2], label='mergeSortDyC', linewidth=3)
        plt.plot(data[:,0], data[:,3], label='upSortDyC', linewidth=3)
        plt.legend()
        return plt.show()
       

#Pruebas finales
#lista=generarPuntosEntrada(20)
#b=upSort(lista)
#c=distanciaMinima(b)
#print(c)
#d=distanciaMinima(b)[0]
#e=distanciaMinimaRec(b)
#f=menorDist(b)
#print(f)
#print (medTiempos(generarPuntosEntrada(150),mergeSort)) 
#print (medTiempos(generarPuntosEntrada(150),upSort)) 
#ComparacionTiempos=datosYGrafico()
#ComparacionTiempos=datosYGrafico(100)
#ComparacionTiempos=datosYGrafico(100,2000)


import re
    
if __name__ == '__main__':
    if len(sys.argv)<2:
        datosYGrafico(x=2,y=500)
    elif len(sys.argv)==2:
        if re.search('.csv',sys.argv[1]):
            lista_puntos=listaDePuntos(sys.argv[1])
            print('Puntos con menor distancia por Fuerza bruta:', distMinDyC(lista_puntos, 'fBruta')[0])
            print('Puntos con menor distancia por mergeSortDyC:',distMinDyC(lista_puntos, mergeSort))
            print('Puntos con menor distancia por upSortDyC:', distMinDyC(lista_puntos,upSort))
        else:
            datosYGrafico(int(sys.argv[1]),y=500)
    elif len(sys.argv)==3:
        datosYGrafico(int(sys.argv[1]),int(sys.argv[2]))
    else:
        print ('El numero de agumentos debe ser menor o igual a 3, siendo argv[1] una entrada de puntos (formato csv) o un numero entero mayor o igual a 2 (minima longitud de lista random de puntos); y siendo argv[2] un numero entero mayor a 2 (maxima longitud de la lista random de puntos)')
    
    
        
    
    
    
    
    
    
    

    
    
    
    
    
    
    
