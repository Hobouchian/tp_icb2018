#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Gomez, Gimena y Hobouchian, Maria Paula
"""
#Ejercicio 2 del TP icb 2018

class ArbolBinario(object):
    """Implementacion de un Arbol binario"""
    
    def __init__(self):
        #Devuelve un arbol vacio, r (raiz), rizq (rama izq) y rder (rama der)
        self.r=None
        self.rizq=None
        self.rder=None
    
    def vacio(self):
        #Indica si el arbol no posee elementos
        return self.r==None and self.rizq==None and self.rder==None
    
    def raiz(self):
        #Devuelve la raiz del arbol
        return self.r

    def bin(self,a,izq,der):
        #Construye el arbol con los valores indicados
        self.r=a
        self.rizq=izq
        self.rder=der
            
    def izquierda(self):
        #Devuelve la rama izquierda
        return self.rizq

    def derecha(self):
        #Devuelve la rama derecha
        return self.rder
    
    def find(self,a):
        #Indica si a esta en el arbol
        if self.vacio():
            return False
        elif self.raiz()==a:
            return True
        elif self.izquierda().find(a):
            return True
        return self.derecha().find(a)
        
    def espejo(self):
        #Devuelve un nuevo arbol espejo
        nuevo=ArbolBinario()
        if self.vacio():
            return nuevo
        else:
            nuevo.bin(self.raiz(),self.derecha().espejo(),self.izquierda().espejo())
            return nuevo
    
    def preorder(self):
        #Devuelve lista de enteros con r y preorden de rizq y rder
        if self.vacio():
            lista=[]    
        else:
            lista=[self.raiz()]+self.izquierda().preorder()+self.derecha().preorder()
        return lista
   
    def posorder(self):
        #Devuelve lista de enteros con posorden de rder y rizq y r
        if self.vacio():
            lista=[]    
        else:
            lista=self.derecha().posorder()+self.izquierda().posorder()+[self.raiz()]
        return lista
    
    def inorder(self):
        #Devuelve lista de enteros como si se aplastara el arbol con r entre rizq y rder
        if self.vacio():
            lista=[]    
        else:
            lista=self.izquierda().inorder()+[self.raiz()]+self.derecha().inorder()
        return lista

#Implementacion de la clase
if __name__ == '__main__':
    #Se prueban los metodos para un arbol vacio
    n0=ArbolBinario()
    if n0.vacio()==True:
        print('Chequea n0 es arbol vacio') 
    if n0.raiz()==None:
        print('Chequea raiz de n0')
    if n0.izquierda()==None:
        print('Chequea rama izquierda de n0')
    if n0.derecha()==None:
        print('Chequea rama derecha de n0')
    ne=n0.espejo()
    if ne.vacio()==True:
        print('Chequea ne es arbol espejo vacio') 
    if ne.raiz()==None:
        print('Chequea raiz de ne')
    if ne.izquierda()==None:
        print('Chequea rama izquierda de ne')
    if ne.derecha()==None:
        print('Chequea rama derecha de ne')
    if n0.find(4)==False:
        print("Chequea find para n0")
    if n0.preorder()==[]:
        print("Chequea preorder de n0")
    if n0.posorder()==[]:
        print("Chequea posorder de n0")
    if n0.inorder()==[]:
        print("Chequea inorder de n0")

    #Se construye un arbol de prueba con n0, bin y la lista de elementos [8,2,1,6,10,9,20] 
    #Orden establecido:8 primer elemento de la lista es la raiz, los valores siguientes menores a rizq y mayores a rder
    #n0:arbol vacio, i, d de izq y der, na:arbol final, ne:arbol espejo

    nii=ArbolBinario()
    nii.bin(1,n0,n0)
    nid=ArbolBinario()
    nid.bin(6,n0,n0)
    ni=ArbolBinario()
    ni.bin(2,nii,nid)
    ndi=ArbolBinario()
    ndi.bin(9,n0,n0)
    ndd=ArbolBinario()
    ndd.bin(20,n0,n0)
    nd=ArbolBinario()
    nd.bin(10,ndi,ndd)
    na=ArbolBinario()
    na.bin(8,ni,nd)

    #Valores del arbol de prueba na
    if na.vacio()==False:
        print('Chequea na arbol de prueba NO es arbol vacio') 
    if na.raiz()==8:
        print('Chequea raiz de na')
    if na.izquierda().raiz()==2:
        print('Chequea raiz de la rama izquierda de na')
    if na.izquierda().izquierda().raiz()==1:
        print('Chequea raiz de la rama izquierda de la rama izquierda de na')
    if na.izquierda().derecha().raiz()==6:
        print('Chequea raiz de la rama derecha de la rama izquierda de na')
    if na.derecha().raiz()==10:
        print('Chequea raiz de la rama derecha de na')
    if na.derecha().izquierda().raiz()==9:
        print('Chequea raiz de la rama izquierda de la rama derecha de na')
    if na.derecha().derecha().raiz()==20:
        print('Chequea raiz de la rama derecha de la rama derecha de na')

    #Valores del arbol espejo ne
    ne=na.espejo()
    if ne.vacio()==False:
        print('Chequea ne arbol espejo NO es arbol vacio') 
    if ne.raiz()==8:
        print('Chequea raiz de ne')
    if ne.izquierda().raiz()==10:
        print('Chequea raiz de la rama izquierda de ne')
    if ne.izquierda().izquierda().raiz()==20:
        print('Chequea raiz de la rama izquierda de la rama izquierda de ne')
    if ne.izquierda().derecha().raiz()==9:
        print('Chequea raiz de la rama derecha de la rama izquierda de ne')
    if ne.derecha().raiz()==2:
        print('Chequea raiz de la rama derecha de ne')
    if ne.derecha().izquierda().raiz()==6:
        print('Chequea raiz de la rama izquierda de la rama derecha de ne')
    if ne.derecha().derecha().raiz()==1:
        print('Chequea raiz de la rama derecha de la rama derecha de ne')

    #Metodo find
    if na.find(6)==True and na.find(4)==False and na.find(20)==True and na.find(22)==False:
        print("Chequea find para na")

    #Listas de ordenamiento
    if na.preorder()==[8, 2, 1, 6, 10, 9, 20]:
        print("Chequea preorder de na ",na.preorder())
    if na.posorder()==[20, 9, 10, 6, 1, 2, 8]:
        print("Chequea posorder de na ",na.posorder())
    if na.inorder()==[1, 2, 6, 8, 9, 10, 20]:
        print("Chequea inorder de na ",na.inorder())
    if ne.preorder()==[8, 10, 20, 9, 2, 6, 1]:
        print("Chequea preorder de ne ",ne.preorder())
    if ne.posorder()==[1, 6, 2, 9, 20, 10, 8]:
        print("Chequea posorder de ne ",ne.posorder())
    if ne.inorder()==[20, 10, 9, 8, 6, 2, 1]:
        print("Chequea inorder de ne ",ne.inorder())
    
    #Si se construye un nuevo arbol utilizando la construcción previa,
    #probamos metodos de ordenamiento para chequear correcto funcionamiento
    n02=ArbolBinario()
    n02.bin(65,n0,n0)
    na2=ArbolBinario()
    na2.bin(33,na,n02)
    ne2=na2.espejo()
    if na2.preorder()==[33,8, 2, 1, 6, 10, 9, 20,65]:
        print("Chequea preorder de na2 ",na2.preorder())
    if na2.posorder()==[65,20, 9, 10, 6, 1, 2, 8,33]:
        print("Chequea posorder de na2 ",na2.posorder())
    if na2.inorder()==[1, 2, 6, 8, 9, 10, 20,33,65]:
        print("Chequea inorder de na2 ",na2.inorder())
    if ne2.inorder()==[65,33,20,10,9,8,6,2,1]:
        print("Chequea inorder de ne2 ",ne2.inorder())